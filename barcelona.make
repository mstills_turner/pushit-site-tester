; Drupal
core = 7.x
api = 2
projects[drupal][type] = "core"
projects[drupal][version] = "7.32"

; Contributed Modules
projects[addressfield][subdir] = "contrib"
projects[addressfield][version] = "1.0-beta5"

projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc5"

projects[admin_views][subdir] = "contrib"
projects[admin_views][version] = "1.2"

projects[autocomplete_deluxe][subdir] = "contrib"
projects[autocomplete_deluxe][version] = "2.0-beta3"

projects[cdn][subdir] = "contrib"
projects[cdn][version] = "2.6"
projects[cdn][patch][] = "https://drupal.org/files/cdn-1942230-18-advagg-hooks.patch"

projects[composer_manager][subdir] = "contrib"
projects[composer_manager][version] = "1.5"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.3"

projects[date][subdir] = "contrib"
projects[date][version] = "2.7"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.3"

projects[entityreference][subdir] = "contrib"
projects[entityreference][version] = "1.1"

projects[devel][subdir] = "contrib"
projects[devel][version] = "1.3"

projects[features][subdir] = "contrib"
projects[features][version] = "2.0"

projects[field_collection][subdir] = "contrib"
projects[field_collection][version] = "1.0-beta5"

projects[field_collection_node_clone][subdir] = "contrib"
projects[field_collection_node_clone][version] = "1.0-beta1"

projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.3"

projects[honeypot][subdir] = "contrib"
projects[honeypot][version] = "1.16"

projects[httprl][subdir] = "contrib"
projects[httprl][version] = "1.14"

projects[imce][subdir] = "contrib"
projects[imce][version] = "1.9"

projects[imce_mkdir][subdir] = "contrib"
projects[imce_mkdir][version] = "1.0"

projects[imce_plupload][subdir] = "contrib"
projects[imce_plupload][version] = "1.2"

projects[imce_wysiwyg][subdir] = "contrib"
projects[imce_wysiwyg][version] = "1.0"

projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "2.3"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.1"

projects[link][subdir] = "contrib"
projects[link][version] = "1.2"

projects[memcache][subdir] = "contrib"
projects[memcache][version] = "1.5"

projects[metatag][subdir] = "contrib"
projects[metatag][version] = "1.0-beta9"

projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "1.8"

projects[new_relic_rpm][subdir] = "contrib"
projects[new_relic_rpm][version] = "1.x-dev"

projects[node_clone][subdir] = "contrib"
projects[node_clone][version] = "1.0-rc2"

projects[nodequeue][subdir] = "contrib"
projects[nodequeue][version] = "2.0-beta1"
projects[nodequeue][patch][] = "patches/nodequeue/nodequeue-NCAA-9547.patch"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = 1.2

projects[plupload][subdir] = "contrib"
projects[plupload][version] = "1.6"

projects[publishcontent][subdir] = "contrib"
projects[publishcontent][version] = 1.2

projects[redirect][subdir] = "contrib"
projects[redirect][version] = "1.0-rc1"
projects[redirect][patch][] = "https://drupal.org/files/redirect.circular-loops.1796596-106.patch"

projects[scald][subdir] = "contrib"
projects[scald][version] = "1.1"

projects[scheduler][subdir] = "contrib"
projects[scheduler][version] = "1.1"

projects[shortcutperrole][subdir] = "contrib"
projects[shortcutperrole][version] = "1.1"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"

projects[token][subdir] = "contrib"
projects[token][version] = "1.5"

projects[transliteration][subdir] = "contrib"
projects[transliteration][version] = "3.1"

projects[variable][subdir] = "contrib"
projects[variable][version] = "2.4"

projects[views][subdir] = "contrib"
projects[views][version] = "3.7"
projects[views][patch][] = "https://drupal.org/files/views-fix-destination-link-for-ajax-1036962-29.patch"

projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "3.2"

projects[xautoload][subdir] = "contrib"
projects[xautoload][version] = "4.3"

; Forked Contrib
projects[coder][type] = "module"
projects[coder][subdir] = "contrib"
projects[coder][download][type] = "git"
projects[coder][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-coder.git"
projects[coder][download][tag] = "v1.0.0"

projects[pagepreview][type] = "module"
projects[pagepreview][subdir] = "contrib"
projects[pagepreview][download][type] = "git"
projects[pagepreview][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-preview.git"
projects[pagepreview][download][tag] = "v1.0.0"

projects[wysiwyg][type] = "module"
projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][download][type] = "git"
projects[wysiwyg][download][url] = "git@bitbucket.org:vgtf/ncaa-drupal-wysiwyg.git"
projects[wysiwyg][download][tag] = "v1.0.0"

; custom apps
projects[gamecenter_app][type] = "module"
projects[gamecenter_app][subdir] = "apps"
projects[gamecenter_app][download][type] = "git"
projects[gamecenter_app][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-gamecenter-app.git"
projects[gamecenter_app][download][tag] = "v1.0.0"

projects[mm_video_hub_app][type] = "module"
projects[mm_video_hub_app][subdir] = "apps"
projects[mm_video_hub_app][download][type] = "git"
projects[mm_video_hub_app][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-video-hub-app.git"
projects[mm_video_hub_app][download][tag] = "v1.2.13"

projects[ncaa_champ_central_app][type] = "module"
projects[ncaa_champ_central_app][subdir] = "apps"
projects[ncaa_champ_central_app][download][type] = "git"
projects[ncaa_champ_central_app][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-champ-central-app.git"
projects[ncaa_champ_central_app][download][tag] = "v1.0.4"

projects[ncaa_mobile_landing_app][type] = "module"
projects[ncaa_mobile_landing_app][subdir] = "apps"
projects[ncaa_mobile_landing_app][download][type] = "git"
projects[ncaa_mobile_landing_app][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-mobile-landing-app.git"
projects[ncaa_mobile_landing_app][download][tag] = "v1.1.1"

projects[scoreboard_app][type] = "module"
projects[scoreboard_app][subdir] = "apps"
projects[scoreboard_app][download][type] = "git"
projects[scoreboard_app][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-scoreboard-app.git"
projects[scoreboard_app][download][tag] = "v1.2.3"

; custom features
projects[ncaa_pushit][type] = "module"
projects[ncaa_pushit][subdir] = "custom"
projects[ncaa_pushit][download][type] = "git"
projects[ncaa_pushit][download][url] = "git@bitbucket.org:mstills_turner/pushit-tester.git"
projects[ncaa_pushit][download][tag] = "v0.0.10"

projects[anagram][type] = "module"
projects[anagram][subdir] = "custom"
projects[anagram][download][type] = "git"
projects[anagram][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-anagram.git"
projects[anagram][download][tag] = "v1.0.1"

projects[articles][type] = "module"
projects[articles][subdir] = "custom"
projects[articles][download][type] = "git"
projects[articles][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-articles.git"
projects[articles][download][tag] = "v1.5.0"

projects[backgrounds][type] = "module"
projects[backgrounds][subdir] = "custom"
projects[backgrounds][download][type] = "git"
projects[backgrounds][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-backgrounds.git"
projects[backgrounds][download][tag] = "v1.0.4"

projects[brackets][type] = "module"
projects[brackets][subdir] = "custom"
projects[brackets][download][type] = "git"
projects[brackets][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-brackets.git"
projects[brackets][download][tag] = "v1.2.3"

projects[ncaa_admin][type] = "module"
projects[ncaa_admin][subdir] = "custom"
projects[ncaa_admin][download][type] = "git"
projects[ncaa_admin][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-admin.git"
projects[ncaa_admin][download][tag] = "v1.2.1"

projects[ncaa_faqs][type] = "module"
projects[ncaa_faqs][subdir] = "custom"
projects[ncaa_faqs][download][type] = "git"
projects[ncaa_faqs][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-faqs.git"
projects[ncaa_faqs][download][tag] = "v1.2.6"

projects[ncaa_footer][type] = "module"
projects[ncaa_footer][subdir] = "custom"
projects[ncaa_footer][download][type] = "git"
projects[ncaa_footer][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-footer.git"
projects[ncaa_footer][download][tag] = "v1.0.1"

projects[ncaa_history][type] = "module"
projects[ncaa_history][subdir] = "custom"
projects[ncaa_history][download][type] = "git"
projects[ncaa_history][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-history.git"
projects[ncaa_history][download][tag] = "v1.0.0"

projects[ncaa_iframe][type] = "module"
projects[ncaa_iframe][subdir] = "custom"
projects[ncaa_iframe][download][type] = "git"
projects[ncaa_iframe][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-iframe.git"
projects[ncaa_iframe][download][tag] = "v1.0.7"

projects[ncaa_multisport][type] = "module"
projects[ncaa_multisport][subdir] = "custom"
projects[ncaa_multisport][download][type] = "git"
projects[ncaa_multisport][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-multisport.git"
projects[ncaa_multisport][download][tag] = "v1.0.4"

projects[ncaa_rankings][type] = "module"
projects[ncaa_rankings][subdir] = "custom"
projects[ncaa_rankings][download][type] = "git"
projects[ncaa_rankings][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-rankings.git"
projects[ncaa_rankings][download][tag] = "v1.2.4"

projects[ncaa_shortcodes][type] = "module"
projects[ncaa_shortcodes][subdir] = "custom"
projects[ncaa_shortcodes][download][type] = "git"
projects[ncaa_shortcodes][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-shortcodes.git"
projects[ncaa_shortcodes][download][tag] = "v1.0.3"

projects[ncaa_ticket_info][type] = "module"
projects[ncaa_ticket_info][subdir] = "custom"
projects[ncaa_ticket_info][download][type] = "git"
projects[ncaa_ticket_info][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-ticket-info.git"
projects[ncaa_ticket_info][download][tag] = "v1.15.0"

projects[ncaa_tickets][type] = "module"
projects[ncaa_tickets][subdir] = "custom"
projects[ncaa_tickets][download][type] = "git"
projects[ncaa_tickets][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-tickets.git"
projects[ncaa_tickets][download][tag] = "v1.3.1"

projects[ncaa_tiles][type] = "module"
projects[ncaa_tiles][subdir] = "custom"
projects[ncaa_tiles][download][type] = "git"
projects[ncaa_tiles][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-tiles.git"
projects[ncaa_tiles][download][tag] = "v1.7.5"

projects[ncaa_topics][type] = "module"
projects[ncaa_topics][subdir] = "custom"
projects[ncaa_topics][download][type] = "git"
projects[ncaa_topics][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-topics.git"
projects[ncaa_topics][download][tag] = "v1.0.8"

projects[ncaa_video][type] = "module"
projects[ncaa_video][subdir] = "custom"
projects[ncaa_video][download][type] = "git"
projects[ncaa_video][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-video.git"
projects[ncaa_video][download][tag] = "v1.21.2"

projects[power_rankings][type] = "module"
projects[power_rankings][subdir] = "custom"
projects[power_rankings][download][type] = "git"
projects[power_rankings][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-power-rankings.git"
projects[power_rankings][download][tag] = "v1.3.3"

projects[smartass][type] = "module"
projects[smartass][subdir] = "custom"
projects[smartass][download][type] = "git"
projects[smartass][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-smartass.git"
projects[smartass][download][tag] = "v1.0.3"

; custom modules
projects[adapters][type] = "module"
projects[adapters][subdir] = "custom"
projects[adapters][download][type] = "git"
projects[adapters][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-adapters.git"
projects[adapters][download][tag] = "v1.0.1"

projects[cvp][type] = "module"
projects[cvp][subdir] = "custom"
projects[cvp][download][type] = "git"
projects[cvp][download][url] = "git@bitbucket.org:vgtf/sdi-cvp.git"
projects[cvp][download][tag] = "v1.0.0"

projects[damit][type] = "module"
projects[damit][subdir] = "custom"
projects[damit][download][type] = "git"
projects[damit][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-damit.git"
projects[damit][download][tag] = "v2.1.2"

projects[embedded_video][type] = "module"
projects[embedded_video][subdir] = "custom"
projects[embedded_video][download][type] = "git"
projects[embedded_video][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-embedded-video.git"
projects[embedded_video][download][tag] = "v1.4.1"

projects[fanshop][type] = "module"
projects[fanshop][subdir] = "custom"
projects[fanshop][download][type] = "git"
projects[fanshop][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-fanshop.git"
projects[fanshop][download][tag] = "v1.0.0"

projects[flynn][type] = "module"
projects[flynn][subdir] = "custom"
projects[flynn][download][type] = "git"
projects[flynn][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-flynn.git"
projects[flynn][download][tag] = "v1.12.6"

projects[gauth][type] = "module"
projects[gauth][subdir] = "custom"
projects[gauth][download][type] = "git"
projects[gauth][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-gauth.git"
projects[gauth][download][tag] = "v1.0.0"

projects[hooker][type] = "module"
projects[hooker][subdir] = "custom"
projects[hooker][download][type] = "git"
projects[hooker][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-hooker.git"
projects[hooker][download][tag] = "v1.0.0"

projects[live_data][type] = "module"
projects[live_data][subdir] = "custom"
projects[live_data][download][type] = "git"
projects[live_data][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-live-data.git"
projects[live_data][download][tag] = "v1.11.15"

projects[mml_desktop][type] = "module"
projects[mml_desktop][subdir] = "custom"
projects[mml_desktop][download][type] = "git"
projects[mml_desktop][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-mml-desktop.git"
projects[mml_desktop][download][tag] = "v2.37.0"

projects[mobile_intercept][type] = "module"
projects[mobile_intercept][subdir] = "custom"
projects[mobile_intercept][download][type] = "git"
projects[mobile_intercept][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-mobile-intercept"
projects[mobile_intercept][download][tag] = "v1.3.0"

projects[ncaa_analytics][type] = "module"
projects[ncaa_analytics][subdir] = "custom"
projects[ncaa_analytics][download][type] = "git"
projects[ncaa_analytics][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-analytics.git"
projects[ncaa_analytics][download][tag] = "v1.5.0"

projects[ncaa_bleacher_report][type] = "module"
projects[ncaa_bleacher_report][subdir] = "custom"
projects[ncaa_bleacher_report][download][type] = "git"
projects[ncaa_bleacher_report][download][url] = "git@bitbucket.org:vgtf/ncaa-bleacher-report.git"
projects[ncaa_bleacher_report][download][tag] = "v1.2.6"

projects[ncaa_chartbeat][type] = "module"
projects[ncaa_chartbeat][subdir] = "custom"
projects[ncaa_chartbeat][download][type] = "git"
projects[ncaa_chartbeat][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-chartbeat.git"
projects[ncaa_chartbeat][download][tag] = "v1.1.2"

projects[ncaa_championships][type] = "module"
projects[ncaa_championships][subdir] = "custom"
projects[ncaa_championships][download][type] = "git"
projects[ncaa_championships][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-championships.git"
projects[ncaa_championships][download][tag] = "v1.0.2"

projects[ncaa_context][type] = "module"
projects[ncaa_context][subdir] = "custom"
projects[ncaa_context][download][type] = "git"
projects[ncaa_context][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-ncaa-context.git"
projects[ncaa_context][download][tag] = "v1.1.6"

projects[ncaa_core][type] = "module"
projects[ncaa_core][subdir] = "custom"
projects[ncaa_core][download][type] = "git"
projects[ncaa_core][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-ncaa-core.git"
projects[ncaa_core][download][tag] = "v1.9.1"

projects[ncaa_courtside][type] = "module"
projects[ncaa_courtside][subdir] = "custom"
projects[ncaa_courtside][download][type] = "git"
projects[ncaa_courtside][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-courtside.git"
projects[ncaa_courtside][download][tag] = "v2.0.0"

projects[ncaa_cvp][type] = "module"
projects[ncaa_cvp][subdir] = "custom"
projects[ncaa_cvp][download][type] = "git"
projects[ncaa_cvp][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-cvp.git"
projects[ncaa_cvp][download][tag] = "v2.1.0"

projects[ncaa_favsies][type] = "module"
projects[ncaa_favsies][subdir] = "custom"
projects[ncaa_favsies][download][type] = "git"
projects[ncaa_favsies][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-favsies.git"
projects[ncaa_favsies][download][tag] = "v1.1.14"

projects[ncaa_feeds][type] = "module"
projects[ncaa_feeds][subdir] = "custom"
projects[ncaa_feeds][download][type] = "git"
projects[ncaa_feeds][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-feeds.git"
projects[ncaa_feeds][download][tag] = "v1.3.5"

projects[ncaa_gigya][type] = "module"
projects[ncaa_gigya][subdir] = "custom"
projects[ncaa_gigya][download][type] = "git"
projects[ncaa_gigya][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-gigya.git"
projects[ncaa_gigya][download][tag] = "v1.2.0"

projects[ncaa_libraries][type] = "module"
projects[ncaa_libraries][subdir] = "custom"
projects[ncaa_libraries][download][type] = "git"
projects[ncaa_libraries][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-libraries.git"
projects[ncaa_libraries][download][tag] = "v1.4.1"

projects[ncaa_link_slide][type] = "module"
projects[ncaa_link_slide][subdir] = "custom"
projects[ncaa_link_slide][download][type] = "git"
projects[ncaa_link_slide][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-link-slide.git"
projects[ncaa_link_slide][download][tag] = "v1.0.2"

projects[ncaa_live_video][type] = "module"
projects[ncaa_live_video][subdir] = "custom"
projects[ncaa_live_video][download][type] = "git"
projects[ncaa_live_video][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-live-video.git"
projects[ncaa_live_video][download][tag] = "v1.2.5"

projects[ncaa_march_madness][type] = "module"
projects[ncaa_march_madness][subdir] = "custom"
projects[ncaa_march_madness][download][type] = "git"
projects[ncaa_march_madness][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-march-madness.git"
projects[ncaa_march_madness][download][tag] = "v1.1.2"

projects[ncaa_microsites][type] = "module"
projects[ncaa_microsites][subdir] = "custom"
projects[ncaa_microsites][download][type] = "git"
projects[ncaa_microsites][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-microsite.git"
projects[ncaa_microsites][download][tag] = "v1.3.1"

projects[ncaa_mobile_detect][type] = "module"
projects[ncaa_mobile_detect][subdir] = "custom"
projects[ncaa_mobile_detect][download][type] = "git"
projects[ncaa_mobile_detect][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-mobile-detect.git"
projects[ncaa_mobile_detect][download][tag] = "v1.3.1"

projects[ncaa_mobile_menu][type] = "module"
projects[ncaa_mobile_menu][subdir] = "custom"
projects[ncaa_mobile_menu][download][type] = "git"
projects[ncaa_mobile_menu][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-mobile-menu.git"
projects[ncaa_mobile_menu][download][tag] = "v1.0.2"

projects[ncaa_mobilefeeds][type] = "module"
projects[ncaa_mobilefeeds][subdir] = "custom"
projects[ncaa_mobilefeeds][download][type] = "git"
projects[ncaa_mobilefeeds][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-mobilefeeds.git"
projects[ncaa_mobilefeeds][download][tag] = "v1.2.3"

projects[ncaa_nav][type] = "module"
projects[ncaa_nav][subdir] = "custom"
projects[ncaa_nav][download][type] = "git"
projects[ncaa_nav][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-nav.git"
projects[ncaa_nav][download][tag] = "v1.2.0"

projects[ncaa_newscred][type] = "module"
projects[ncaa_newscred][subdir] = "custom"
projects[ncaa_newscred][download][type] = "git"
projects[ncaa_newscred][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-newscred.git"
projects[ncaa_newscred][download][branch] = "v1.1.1"

projects[ncaa_nodequeue][type] = "module"
projects[ncaa_nodequeue][subdir] = "custom"
projects[ncaa_nodequeue][download][type] = "git"
projects[ncaa_nodequeue][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-ncaa-nodequeue.git"
projects[ncaa_nodequeue][download][tag] = "v1.0.1"

projects[ncaa_optimizely][type] = "module"
projects[ncaa_optimizely][subdir] = "custom"
projects[ncaa_optimizely][download][type] = "git"
projects[ncaa_optimizely][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-optimizely.git"
projects[ncaa_optimizely][download][tag] = "v1.0.1"

projects[ncaa_pages][type] = "module"
projects[ncaa_pages][subdir] = "custom"
projects[ncaa_pages][download][type] = "git"
projects[ncaa_pages][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-pages.git"
projects[ncaa_pages][download][tag] = "v1.6.1"

projects[ncaa_schools][type] = "module"
projects[ncaa_schools][subdir] = "custom"
projects[ncaa_schools][download][type] = "git"
projects[ncaa_schools][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-schools.git"
projects[ncaa_schools][download][tag] = "v1.3.2"

projects[ncaa_search][type] = "module"
projects[ncaa_search][subdir] = "custom"
projects[ncaa_search][download][type] = "git"
projects[ncaa_search][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-search.git"
projects[ncaa_search][download][tag] = "v1.2.0"

projects[ncaa_social][type] = "module"
projects[ncaa_social][subdir] = "custom"
projects[ncaa_social][download][type] = "git"
projects[ncaa_social][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-social.git"
projects[ncaa_social][download][tag] = "v1.4.0"

projects[ncaa_sports][type] = "module"
projects[ncaa_sports][subdir] = "custom"
projects[ncaa_sports][download][type] = "git"
projects[ncaa_sports][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-sports.git"
projects[ncaa_sports][download][tag] = "v1.3.0"

projects[ncaa_standings][type] = "module"
projects[ncaa_standings][subdir] = "custom"
projects[ncaa_standings][download][type] = "git"
projects[ncaa_standings][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-standings.git"
projects[ncaa_standings][download][tag] = "v1.5.0"

projects[ncaa_stats][type] = "module"
projects[ncaa_stats][subdir] = "custom"
projects[ncaa_stats][download][type] = "git"
projects[ncaa_stats][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-stats.git"
projects[ncaa_stats][download][tag] = "v1.1.5"

projects[ncaa_ups_tpi][type] = "module"
projects[ncaa_ups_tpi][subdir] = "custom"
projects[ncaa_ups_tpi][download][type] = "git"
projects[ncaa_ups_tpi][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-ups-tpi.git"
projects[ncaa_ups_tpi][download][tag] = "v0.1.13"

projects[ncaa_wysiwyg][type] = "module"
projects[ncaa_wysiwyg][subdir] = "custom"
projects[ncaa_wysiwyg][download][type] = "git"
projects[ncaa_wysiwyg][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-wysiwyg.git"
projects[ncaa_wysiwyg][download][tag] = "v1.0.5"

projects[ncaa_youtube][type] = "module"
projects[ncaa_youtube][subdir] = "custom"
projects[ncaa_youtube][download][type] = "git"
projects[ncaa_youtube][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-youtube.git"
projects[ncaa_youtube][download][tag] = "v1.1.1"

projects[path_rule][type] = "module"
projects[path_rule][subdir] = "custom"
projects[path_rule][download][type] = "git"
projects[path_rule][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-path-rule.git"
projects[path_rule][download][tag] = "v1.0.3"

projects[playlists][type] = "module"
projects[playlists][subdir] = "custom"
projects[playlists][download][type] = "git"
projects[playlists][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-playlists.git"
projects[playlists][download][tag] = "v1.2.4"

projects[sdi_image_wrangler][type] = "module"
projects[sdi_image_wrangler][subdir] = "custom"
projects[sdi_image_wrangler][download][type] = "git"
projects[sdi_image_wrangler][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-image-wrangler.git"
projects[sdi_image_wrangler][download][tag] = "v1.0.3"

projects[sdi_seo][type] = "module"
projects[sdi_seo][subdir] = "custom"
projects[sdi_seo][download][type] = "git"
projects[sdi_seo][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-seo.git"
projects[sdi_seo][download][tag] = "v1.0.4"

projects[sdi_videomgmt][type] = "module"
projects[sdi_videomgmt][subdir] = "sdi"
projects[sdi_videomgmt][download][type] = "git"
projects[sdi_videomgmt][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-sdi-videomgmt.git"
projects[sdi_videomgmt][download][tag] = "v1.1.2"

projects[settings][type] = "module"
projects[settings][subdir] = "custom"
projects[settings][download][type] = "git"
projects[settings][download][url] = "git@bitbucket.org:vgtf/drupal-settings.git"
projects[settings][download][tag] = "7.x-1.0"

projects[umbel][type] = "module"
projects[umbel][subdir] = "custom"
projects[umbel][download][type] = "git"
projects[umbel][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-umbel.git"
projects[umbel][download][tag] = "v1.0.2"

projects[validate][type] = "module"
projects[validate][subdir] = "custom"
projects[validate][download][type] = "git"
projects[validate][download][url] = "git@bitbucket.org:vgtf/drupal-validate.git"
projects[validate][download][tag] = "7.x-1.0"

projects[vod][type] = "module"
projects[vod][subdir] = "custom"
projects[vod][download][type] = "git"
projects[vod][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-vod.git"
projects[vod][download][tag] = "v1.9.0"

; Themes
projects[ncaa][type] = "theme"
projects[ncaa][download][type] = "git"
projects[ncaa][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-themes.git"
projects[ncaa][download][tag] = "v1.9.7"

; Libraries
libraries[ckeditor][download][type] = "git"
libraries[ckeditor][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-ckeditor.git"
libraries[ckeditor][download][tag] = "v1.0.1"

libraries[composer_libraries][download][type] = "git"
libraries[composer_libraries][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-composer-libraries.git"
libraries[composer_libraries][download][tag] = "v1.0.0"
libraries[composer_libraries][directory_name] = "vendor"

libraries[plupload_lib][download][type] = "get"
libraries[plupload_lib][download][url] = "http://i2.turner.ncaa.com/dr/ncaa/ncaa7/release/sites/default/files/libraries/plupload.tar.gz"
libraries[plupload_lib][directory_name] = "plupload"

libraries[sass-bootstrap][download][type] = "git"
libraries[sass-bootstrap][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-sass-bootstrap.git"
libraries[sass-bootstrap][download][tag] = "v1.1.0"

libraries[tests][download][type] = "git"
libraries[tests][download][url] = "git@bitbucket.org:vgtf/ncaa-barcelona-tests.git"
libraries[tests][download][tag] = "v1.1.0"
libraries[tests][destination] = ""

libraries[google-api-php-client][download][type] = "git"
libraries[google-api-php-client][download][url] = "https://github.com/google/google-api-php-client.git"
libraries[google-api-php-client][download][branch] = "1.0.5-beta"